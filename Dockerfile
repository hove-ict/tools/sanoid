FROM docker.io/ubuntu AS builder

ARG DEBIAN_FRONTEND="noninteractive"

RUN apt-get update && apt-get install -y \
    debhelper \
    git \
    libcapture-tiny-perl \
    libconfig-inifiles-perl \
    pv \
    lzop \
    mbuffer \
    build-essential

WORKDIR /build/sanoid

RUN git clone --depth 1 https://github.com/jimsalterjrs/sanoid.git /build/sanoid && \
    git checkout $(git tag | grep "^v" | tail -n 1)

# Temporary for https://github.com/jimsalterjrs/sanoid/issues/921
ARG GIT_AUTHOR_NAME="Your Name"
ARG GIT_AUTHOR_EMAIL="your.email@example.com"
ARG GIT_COMMITTER_NAME="Your Name"
ARG GIT_COMMITTER_EMAIL="your.email@example.com"
RUN git remote add fork https://github.com/phreaker0/sanoid.git && \
    git fetch fork && \
    git cherry-pick 4d39e3921768fac5f5dc6cb7ce24b3cc804b5c8c

RUN ln -s packages/debian . && \
    dpkg-buildpackage -uc -us

FROM docker.io/ubuntu

ARG DEBIAN_FRONTEND="noninteractive"

COPY --from=builder /build/sanoid_*_all.deb /tmp/

RUN apt-get update && apt-get install -y /tmp/*.deb zfsutils-linux

COPY --from=builder /build/sanoid/sanoid.conf /etc/sanoid/sanoid.example.conf
